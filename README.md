# express-wkd

Express middleware for Web Key Directory

## Install

```
npm install express-wkd
```

or

```
yarn add express-wkd
```

## Usage

`express-wkd` supports both direct mode and advanced mode as described in the [Web Key Directory spec](https://datatracker.ietf.org/doc/draft-koch-openpgp-webkey-service/).

By default, `express-wkd` handles both methods.

### Default usage

```
const wkd = require('express-wkd');

app.use(wkd());
```

### Options: domain

Since the domain name is included in the advanced method path, you must specify
it in order for advanced method to work.

```
app.use(wkd({ domain: 'example.com' }));
```

### Options: method

To only use the `direct` or `advanced` method, set the `method` option
(defaults to `all`).

```
app.use(wkd({ method: 'direct' }));   // direct method only
app.use(wkd({ method: 'advanced' })); // advanced method only
app.use(wkd({ method: 'all' }));      // all methods
```

### Options: pathKeys

Set a custom path for the keys (defaults to `./keys/`).

```
app.use(wkd({ pathKeys: './assets/keys/' }));
```

## Cryptographic key files

`express-wkd` expects the (non-armored) cryptographic key files to be stored in
the `keys` subdirectory relative to the script that called it.

So, if `/index.js` calls `express-wkd`, the keys are expected to be stored in
`/keys/`. This behavior can be changed using the `pathKeys` option.

The cryptographic key files must be non-armored, must have the `.pgp` file
extension and the filename must be equal to the WKD local part. To compute the
WKD local part, use the [Keyoxide WKD tool](https://keyoxide.org/util/wkd).

## Testing

```
npm install
npm test
```

or

```
yarn install
yarn test
```
