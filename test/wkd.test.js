// Copyright (C) 2020 Yarmo Mackenbach
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
//
// Also add information on how to contact you by electronic and paper mail.
//
// If your software can interact with users remotely through a computer network,
// you should also make sure that it provides a way for users to get its source.
// For example, if your program is a web application, its interface could display
// a "Source" link that leads users to an archive of the code. There are many
// ways you could offer source, and different solutions will be better for different
// programs; see section 13 for the specific requirements.
//
// You should also get your employer (if you work as a programmer) or school,
// if any, to sign a "copyright disclaimer" for the program, if necessary. For
// more information on this, and how to apply and follow the GNU AGPL, see <https://www.gnu.org/licenses/>.

const expect = require('chai').expect
const assert = require('chai').assert
const express = require('express')
const request = require('supertest')
const wkd = require('../index')

describe('wkd', function() {
  let app

  beforeEach(function() {
    app = express()
  })

  it('should export a function which returns a middleware', function() {
    let middleware = wkd()

    expect(wkd).to.be.a('function')
    expect(middleware).to.be.a('function')
    expect(middleware).to.have.length(3)
  })

  it('should return 404 for unused paths', function (done) {
    let middleware = wkd()
    app.use(middleware)

    request(app)
      .get('/.well-known/openpgpkey')
      .expect(404, done)
  })

  it('should return an empty policy file in direct mode', function (done) {
    let middleware = wkd()
    app.use(middleware)

    request(app)
      .get('/.well-known/openpgpkey/policy')
      .expect('Content-Length', '0')
      .expect(200, done)
  })

  it('should return an empty policy file in advanced mode', function (done) {
    let middleware = wkd({ domain: "domain.org" })
    app.use(middleware)

    request(app)
      .get('/.well-known/openpgpkey/domain.org/policy')
      .expect('Content-Length', '0')
      .expect(200, done)
  })

  it('should return 404 in advanced mode when wrong domain is set', function (done) {
    let middleware = wkd({ domain: "domain.com" })
    app.use(middleware)

    request(app)
      .get('/.well-known/openpgpkey/domain.org/policy')
      .expect(404, done)
  })

  it('should return the requested key when it exists', function (done) {
    let middleware = wkd()
    app.use(middleware)

    request(app)
      .get('/.well-known/openpgpkey/hu/gq1wjayc46f3xsjxad9da3889gzfch5z')
      .set('Accept', 'application/octet-stream')
      .expect('Content-Type', 'application/octet-stream')
      .expect(200, done)
  })

  it('should return 404 when the requested does not exist', function (done) {
    let middleware = wkd()
    app.use(middleware)

    request(app)
      .get('/.well-known/openpgpkey/hu/z5hcfzg9883ad9daxjsx3f64cyajw1qg')
      .expect(404, done)
  })

  it('should support HEAD method', function (done) {
    let middleware = wkd()
    app.use(middleware)

    request(app)
      .head('/.well-known/openpgpkey/hu/gq1wjayc46f3xsjxad9da3889gzfch5z')
      .expect(response => {
        assert.equal(Object.keys(response.body).length, 0, 'Expected an empty body.')
      })
      .expect(200, done)
  })


})
