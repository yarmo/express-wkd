// Copyright (C) 2020 Yarmo Mackenbach
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
//
// Also add information on how to contact you by electronic and paper mail.
//
// If your software can interact with users remotely through a computer network,
// you should also make sure that it provides a way for users to get its source.
// For example, if your program is a web application, its interface could display
// a "Source" link that leads users to an archive of the code. There are many
// ways you could offer source, and different solutions will be better for different
// programs; see section 13 for the specific requirements.
//
// You should also get your employer (if you work as a programmer) or school,
// if any, to sign a "copyright disclaimer" for the program, if necessary. For
// more information on this, and how to apply and follow the GNU AGPL, see <https://www.gnu.org/licenses/>.

const path = require('path')
const fs = require('fs')

module.exports = function(options) {
  return function(req, res, next) {
    // Options
    let opts = options || {}
    opts.pathKeys = opts.pathKeys || './keys/'
    opts.domain = opts.domain || null
    opts.method = opts.method || 'all'

    let re, match

    const allowDirect = opts.method === 'direct' || opts.method === 'all'
    const allowAdvanced = opts.method === 'advanced' || opts.method === 'all'

    // Verify request path
    re = new RegExp(`^\/\.well-known\/openpgpkey`, 'i')
    if(!re.test(req.url)) {
      return next()
    }

    // Handle policy requests
    re = new RegExp(`^\/\.well-known\/openpgpkey\/policy`, 'i')
    if(allowDirect && re.test(req.url)) {
      res.status(200).send('')
      return
    }
    re = new RegExp(`^\/\.well-known\/openpgpkey\/${opts.domain}\/policy`, 'i')
    if(allowAdvanced && re.test(req.url)) {
      res.status(200).send('')
      return
    }

    // Handle direct key requests
    re = new RegExp(`^\/\.well-known\/openpgpkey\/hu\/([a-zA-Z0-9]*)(?:\\?l\\=(.*))?`, 'i')
    if(allowDirect && re.test(req.url)) {
      match = req.url.match(re)
      let pathFile = path.join(opts.pathKeys, `${match[1]}.pgp`)
      if (fs.existsSync(pathFile)) {
        let data = fs.readFileSync(pathFile)
        res.contentType('application/octet-stream')
        res.status(200).send(data)
        return
      }
    }

    // Handle advanced key requests
    re = new RegExp(`^\/\.well-known\/openpgpkey\/${opts.domain}\/hu\/([a-zA-Z0-9]*)(?:\\?l\\=(.*))?`, 'i')
    if(allowAdvanced && re.test(req.url)) {
      match = req.url.match(re)
      let pathFile = path.join(opts.pathKeys, `${match[1]}.pgp`)
      if (fs.existsSync(pathFile)) {
        let data = fs.readFileSync(pathFile)
        res.contentType('application/octet-stream')
        res.status(200).send(data)
        return
      }
    }

    // Default response
    res.sendStatus(404)
  }
}
